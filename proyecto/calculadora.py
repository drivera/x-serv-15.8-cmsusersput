#!/usr/bin/python3

import sys
import decimal
import operator

# Map string to operation
operations_map = {
    "sumar": operator.add,
    "restar": operator.sub,
    "multiplicar": operator.mul,
    "dividir": operator.truediv,
}

def todec(op1,op2):
    try:
        op1 = decimal.Decimal(op1)
        op2 = decimal.Decimal(op2)
    except decimal.InvalidOperation as e:
        raise decimal.InvalidOperation("Error: ARGs must be inbounds python decimal number\n{}".format(e))
    return(op1,op2)

def calculator(operation, op1, op2):
# Check entry:
    if operation not in operations_map:
        raise KeyError("Error: Operation not supported")

    op1,op2=todec(op1,op2)
# Main
    try:
        result =  operations_map[operation](op1,op2)
    except decimal.Overflow:
        raise OverflowError("Error: Operation overflow")
    except decimal.DivisionByZero:
        result = "Division by Zero not allowed"
    return result

if __name__=="__main__":
    if(len(sys.argv) != 4):
        sys.exit("Usage error: $ calc.py Operation Arg1 Arg2")
    try:
        result = calculator(sys.argv[1],sys.argv[2],sys.argv[3])
    except Exception as e:
        exit(e)
    else:
        print("result = {}".format(result))
        exit(0)
