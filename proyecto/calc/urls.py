from django.urls import path

from . import views

urlpatterns = [
    path('',views.index),
    path('calcular/<str:op>/<str:op1>/<str:op2>',views.calcular),
    path('sumar/<int:op1>/<int:op2>',views.add),
    path('restar/<int:op1>/<int:op2>',views.sub),
    path('multiplicar/<int:op1>/<int:op2>',views.multi),
    path('dividir/<int:op1>/<int:op2>',views.div),
]
